__author__ = 'sergioska'

import sys
from libs.settings import Local
from libs.evercode import EverCode
from libs.pygCode import PygCode

if __name__ == "__main__":

	if len(sys.argv) < 4:
		sys.exit('Usage: %s language title filename' % sys.argv[0])

	evercode = EverCode(Local())

	code = PygCode()
	''' set language '''
	code.setLexer(sys.argv[1])

	''' set content '''
	source = code.getFileContent(sys.argv[3])
	body = code.getHighLightCode(source)

	''' set title '''
	title = sys.argv[2]

	''' connect to evercode account '''
	evercode.setClient();
	''' send snippet '''
	evercode.makeNote(title, body);