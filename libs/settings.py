__author__ = 'sergioska'

from configurations.Settings import Setting

class Local(Setting):
	def __init__(self):
		self.local = True
		self.developer_token = ''
		self.sandbox = False

class Local_Dev(Setting):
	def __init__(self):
		self.local = True
		self.developer_token = ''
		self.sandbox = True

class Dev(Setting):
	def __init__(self):
		self.consumer_key = ""
		self.consumer_secret = ""
		self.sandbox = False
		self.authorize_url = "https://sandbox.evernote.com/OAuth.action?oauth_token=OAUTH_TOKEN"
		'''Settings.callback_url = "https://sandbox.evernote.com/oauth?oauth_consumer_key=en_oauth_test&oauth_signature=1ca0956605acc4f2%26&oauth_signature_method=PLAINTEXT&oauth_timestamp=1288364369&oauth_nonce=d3d9446802a44259&oauth_callback=https%3A%2F%2Ffoo.com%2Fsettings%2Findex.php%3Faction%3DoauthCallback"'''

class Prod(Setting):
	def __init__(self):
		Settings.consumer_key = "consumer_key"
		Settings.consumer_secret = "consumer_secret"
		Settings.sandbox = False
		Settings.callback_url = "url"
